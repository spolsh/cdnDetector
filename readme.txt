Opis zawartosci katalogu
cdnDetector/        - katalog narzędzia
	/java8/	        - czesc narzecia do wykrywania zasobow, ktore moga korzystac z CDN
		/bin/ 	    - pliki binarne
		/data/	    - pliki tekstowe z danymi wejściowymi i wyjściowymi
		/META-INF/
		/src/	    - zrodla
		/cdnDetector_java.iml
	/nodejs/        - czesc narzedzia do wykrywania rekurencyjnych serwerow DNS
		/app/	    - glowny katalog narzedzia
		/data/	    - pliki tekstowe z danymi wejściowymi i wyjściowymi
		/package.json
	/CDNDetector    - dokumentacja.pdf - dokumentacja narzedzia CDNDetector
	/readme.txt
