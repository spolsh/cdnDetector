/// 
/// Filename    :   cdn.js
/// Content     :   tool for checking if given urls use cdn
///
/// Created     :   November 13, 2015
/// Authors     :   Michal Klos
///
/// Copyright   :   Copyright 2015 PWr IiBI
///

console.log( 'cdn detector start' );

/// input paths
// IN
var dnsFileIn = './data/recursiveDnsList.txt';
    urlFileIn = './data/urlList.txt';

/// output paths
// OUT
var fileOut = './data/cdnDetectorResults.txt';

/// loading useful modules
// requires
var fs = require( 'fs' ),
    Q = require( 'q' ),
    utils = require( './utils' );
    
/// creating deffered versions of callback functions
var	readFileQ = Q.denodeify( fs.readFile ),
	writeFileQ = Q.denodeify( fs.writeFile ),
	appendFileQ = Q.denodeify( fs.appendFile ),
	unlinkQ = Q.denodeify( fs.unlink );

/// helper function for removing duplicates from array
/// extends Array prototype
Array.prototype.removeDuplicates = function () {
  var temp=new Array();
  label:for(i=0;i<this.length;i++){
        for(var j=0; j<temp.length;j++ ){//check duplicates
            if(temp[j]==this[i])//skip if already present 
               continue label;      
        }
        temp[temp.length] = this[i];
  }
  return temp;
 } 

/// makes dns query for given network resource to provided dns servers
function checkUrlIpsQ( url, dnsServers ) {
    
	/// helper for removing first element from an Array
	function removeFirstElem( arr ) {
        if ( arr ) {
            arr.splice( 0, 1 );
        } else {
            console.log( '###', url, 'arr is null' );
        }
        return arr; 
    };
	
	/// helper for parsing ip addresses from raw command output
    var getIpAddress = function ( nsloopupResoponse ) {                
        
        var clear = nsloopupResoponse.replace(/\s+/g,"");
        // console.log( 'clear', clear );
        
        // TODO: refactor
        var regexResult = clear.match(/Address:\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}/g);
        
        if ( regexResult ) {
            return [].concat.apply([], removeFirstElem( regexResult )
			.map( function ( el ) { 
				return el.match(/\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}/g); 
			} ) );            
        } else {
            return [];
        }        
    }

    // console.log( 'checkUrlsQ:', url, 'dnsLen', dnsServers.length );
    
    var dnsQueries = dnsServers.map( function ( dns ) {
		// console.log( 'dns', dns );
		
		// primary dns query
		/// execute nslookup with primary dns ip
		return utils.executeQ( "nslookup " + url[0] + " " + dns[0] ) 
                .then( function ( nsloopupResoponse ) {
                    // console.log( 'nsloopupResoponse1', nsloopupResoponse );
                    
                    var ipAddress = getIpAddress( nsloopupResoponse );
                    console.log( 'ipAddress from ', dns[0], 'for', url[0], ipAddress );
                    
					/// if resulting address is correct return it
					/// otherwise execute nslookup with secondary dns ip
                    if ( ipAddress ) {
						return ipAddress;
                        
                    } else {
                        // primary failed => do secondary dns query
						/// execute nslookup with secondary dns ip
                        return utils.executeQ( "nslookup " + url[0] + " " + dns[1] )
                            .then( function( altNsloopupResoponse ) {
                                // console.log( 'nsloopupResoponse2', nsloopupResoponse );
                                
                                var ipAddress2 = getIpAddress( nsloopupResoponse );
                                console.log( 'ipAddress2 from ', dns[1], 'for', url[0], ipAddress2 );
                                
                                return ipAddress2;
                            } );
                    }
                } );
    } );
    
	/// wait for all dns queries to complete and process the results
    return Q.all( dnsQueries )
		.then( function ( ips ) {
			var ips = [].concat.apply([], ips );
			console.log( 'ips', url[0], 'ipLen', ips.length );
            
			var allLen = ips.length;
			var succesfullIps = ips.filter( function ( ip ) { return ip != false; } );            
			var succesfullLen = succesfullIps.length;            
			var ipsSet = ips.removeDuplicates();
			var sameLen = succesfullLen - ipsSet.length;
			
			/// format output with measures
			var out = utils.concat( [ url[0], url[1], succesfullIps > 1 ? 'Y' : 'N', allLen, sameLen ] );
			console.log( 'out', out );
            
			return out;
		} );
}

/// empty output file
writeFileQ( fileOut, '' )
    .then( function () {
		
		/// read file with dns servers list
		readFileQ( dnsFileIn, 'utf8' )
			.then( function( dnsServersContent ) { 
				
				/// split dns servers by delimiter
				var dnsServers = dnsServersContent.split( /\r?\n/ )
					.map( function ( dnsLine ) {
						return dnsLine.split( utils.fileDefaultDelimiter );
					} );
					
				/// remove empty dns entries
				dnsServers = dnsServers.filter( function ( dns ) {
					return dns && dns.length && dns[0] != '';
				} );
				
				console.log( 'dns', dnsServers );
				
				// var rl = require( 'readline' ).createInterface( {
				// 	input : require( 'fs' ).createReadStream( urlFileIn ),
				// 	terminal : false
				// } );
				
				/// read urls line by line
				readFileQ( urlFileIn, 'utf8' )
					.then( function ( urlsContent ) {
						
						/// split urls data
						var urls = urlsContent.split( /\r?\n/ )
							.map( function ( urlLine ) {
								return urlLine.split( utils.fileDefaultDelimiter );
							} );
						
						/// for each url resource perform dns queries 
						urls.forEach( function ( url, index ) {
							// console.log( 'url', url );
							
							checkUrlIpsQ( url, dnsServers )
								.then( function ( line ) {
									/// print output and append to output file
									console.log( 'line', line );
									appendFileQ( fileOut, line + '\r\n' );
								} );
						} );						
					} );
					
			} );
	} )
	.catch( function ( err ) {
		console.log( 'err', err );
	} );