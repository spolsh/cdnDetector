/// 
/// Filename    :   utility.js
/// Content     :   helper functions
///
/// Created     :   November 12, 2015
/// Authors     :   Michal Klos
///
/// Copyright   :   Copyright 2015 PWr IiBI
///


console.log( 'utils' );

/// fetch used modules
var sys = require( 'sys' ),
    exec = require( 'child_process' ).exec,
    Q = require( 'q' );

/// export defualt string delimiter
exports.fileDefaultDelimiter = ';';
    
/// export helper function for string concatenation
exports.concat = function ( stringArray, delimiter ) {
    delimiter = delimiter || exports.fileDefaultDelimiter;
    var output = '';
    
    for ( var i = 0, l = stringArray.length; i < l; ++i ) {
        output += stringArray[i];
        
        // do not add delimiter after last element, skip it
        if ( i != l - 1 ) {
            output += delimiter;
        }
    }
    return output;
}

/// export helper function deffered shell command execution
exports.executeQ = function executeQ( command ) {    
    var deferred = Q.defer();
    
    exec( command, function( error, stdout, stderr ) {
        if ( error ) {
            deferred.reject( error ); 
        } else {
            deferred.resolve( stdout );
        }
    } );
    
    return deferred.promise;
};