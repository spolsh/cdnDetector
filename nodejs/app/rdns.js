/// 
/// Filename    :   rdns.js
/// Content     :   tool for checking if dns server is recursive
///
/// Created     :   November 12, 2015
/// Authors     :   Michal Klos
///
/// Copyright   :   Copyright 2015 PWr IiBI
///

console.log( 'recursive dns verifier start' );

/// input variables and paths
// IN
var verbose = true,
    fileIn = './data/rawDnsList.txt',
    testDomain = 'www.cnn.com.';


/// out path
// OUT
var fileOut = './data/recursiveDnsList.txt';


/// loading useful modules
// requires
var fs = require( 'fs' ),
    Q = require( 'q' ),
    utils = require( './utils' );
    
/// creating deffered versions of callback functions
var readFileQ = Q.denodeify( fs.readFile ),
    writeFileQ = Q.denodeify( fs.writeFile ),
    appendFileQ = Q.denodeify( fs.appendFile ),
    unlinkQ = Q.denodeify( fs.unlink );


/// querying dns server
/// dnsline: primary, secondary addresses and label
/// returns: promise, if dnsServer is recursive
function isDnsRecursiveQ( dnsLine ) {    
       
   /// helper function for parsing query output
    var isRecursiveDns = function ( hostCommandResponse ) {                
        return !( hostCommandResponse.match( /REFUSED/g ) );
    }
    
    var deferred = Q.defer();      
	
	/// get dns server addresses from dnsLine
    var parts = dnsLine.split( utils.fileDefaultDelimiter ),   
        primary = parts[0],
        secondary = parts[1];    
        
	/// promise execution of 2 async commandline tasks
    Q.all( [
            utils.executeQ( "host " + testDomain + " " + primary ),
            utils.executeQ( "host " + testDomain + " " + secondary ),
        ] )
        .then( function ( hostResponses ) {            
			/// check if responses pass recursive check
            var recursives = hostResponses.map( function ( hostResponse ) { return isRecursiveDns( hostResponse ); } )
                .filter( function ( result ) { return result; } );            
            
			/// resolve promise: true when is recursive, false otherwise
            deferred.resolve( recursives.length > 0 );            
        } )
        .catch( function ( error ) {
            deferred.resolve( false );            
        } );
       
    return deferred.promise;
}



/// empty the output file
writeFileQ( fileOut, '' )
    .then( function () {
		
		/// create input reading stream for dns servers
		var rl = require( 'readline' ).createInterface( {
			input : require( 'fs' ).createReadStream( fileIn ),
			terminal : false
         } );
        
		/// read each line of file with dns servers
		rl.on( 'line' , function ( line ) {
			// started++;
			// console.log( 'Line from file:', line );
			/// for each line execute recursive check
			isDnsRecursiveQ( line )
				.then( function ( isRecursive ) {
					if ( verbose ) {
						console.log( isRecursive ? 'recursive\t' : 'failed\t\t', line );
					}                                    
                    
					/// append result to output file
					return appendFileQ( fileOut, line + '\r\n' );                
				} );
                // .then( function ( err ) {
                //     done++;
                //     console.log( 'done', done, '/', started );
                // });                
        });

        // rl.on( 'close', function() {
        //     console.log( 'closed' );
        // } );       
    } )
    .catch( function ( err ) {
        console.log( 'got err', err );
    } );