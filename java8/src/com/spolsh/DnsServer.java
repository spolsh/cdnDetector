package com.spolsh;

/**
 * Created by spolsh on 2016-01-16.
 */

/**
 * Structure for keeping dns server data
 */
public class DnsServer {

    public final String primaryIp;
    public final String secondaryIp;
    public final String label;

    /**
     * Default constructor
     * @param primaryIp
     * @param secondaryIp
     * @param label
     */
    public DnsServer( String primaryIp, String secondaryIp, String label ) {
        this.primaryIp = primaryIp;
        this.secondaryIp = secondaryIp;
        this.label = label;
    }

    /**
     * Create DnsServer object parsed from string line
     * @param csvLine input string
     * @return parsed object
     */
    public static DnsServer fromCSV( String csvLine ) {
        String[] parts = csvLine.split( ";" );
        return new DnsServer( parts[0], parts[1], parts[2] );
    }
}
