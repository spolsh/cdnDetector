/************************************************************************************

 Filename    :   Main.java
 Content     :   cdnDetector, java logic calling nslooup via commandline process

 Created     :   January 17, 2016
 Authors     :   Michal Klos

 Copyright   :   Copyright 2015 PWr IiBI

 ************************************************************************************/


package com.spolsh;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Main class of the cdnDetector
 */
public class Main {

    /**
     * Program control flags
     */
    private static final boolean SHOW_CONSOLE_LOG = false;

    private static final boolean RUN_CDN_DETECTOR = true;
    private static final boolean RUN_IP_PARSING_TEST = false;

    /**
     * path to input and output files
     */
    private static final String FILE_DNS_LIST   = "./data/recursiveDnsList.txt";
    private static final String FILE_URL_LIST   = "./data/urlList.txt";
    private static final String FILE_RESULT     = "./data/cdnDetectorResults.txt";

    /**
     * start of the program
     * @param args unused command line arguments
     */
    public static void main( String[] args ) {

        System.out.println( "cdnDetector start" );

        if ( RUN_IP_PARSING_TEST ) {
            testParsingIp();
        }

        if ( RUN_CDN_DETECTOR ) {
            cdnDetector();
        }

        System.out.println( "cdnDetector end" );
    }

    /**
     * module for reading and writing data.
     * executes ip query fore every resource and saves the result
     */
    static void cdnDetector() {

        Writer output = null;

        try {

            /**
             * create input and output streams
             */

            output = new BufferedWriter( new FileWriter( FILE_RESULT, true ) );

            ArrayList<DnsServer> dnsServers     = readDnsList( FILE_DNS_LIST );
            ArrayList<UrlResource> urlResources = readUrlList( FILE_URL_LIST );

            clearFile( FILE_RESULT );

            /**
             * get ip query results for each resource on list
             */
            for ( int i = 0; i < urlResources.size(); ++i ) {

                UrlResource res = urlResources.get( i );
                System.out.printf( "\n[%d/%d] resource %s\n", i, urlResources.size(), res.url );

                String result = getIpsFromDns( res, dnsServers );
                System.out.print( "\n\tresult> " + result + "\n\n" );

                /**
                 * save result to output
                 */
                output.append( result );

                if ( i < urlResources.size() - 1 ) {
                    output.append( "\n" );
                }
            }

        } catch ( IOException e ) {
            e.printStackTrace();

        } finally {

            /**
             * close output stream
             */

            if ( output != null ) {
                try {
                    output.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Given result perform dns queries and calculate result
     * @param res network resource, url
     * @param dnsServers list of dns serwers tho whom ip queries are send
     * @return  formated result
     */
    static String getIpsFromDns(UrlResource res, List<DnsServer> dnsServers ) {

        System.out.printf( "\tgetIpsFromDns %s\n", res.url );

        String result = "";
        ArrayList<Object> resIps = new ArrayList<>();
        int failedRequests = 0;
        int duplicateIps = 0;

        /**
         * query all servers
         */
        for ( int i = 0; i < dnsServers.size(); ++i ) {

            DnsServer dns = dnsServers.get( i );
            System.out.printf( "\t[%d/%d] dns query %s\n", i, dnsServers.size(), dns.primaryIp );

            /**
             * get all ip results from dns server
             */
            ArrayList<String> ipsFromDns = handleExceptionsGetIpsFromDns( res.url, dns.primaryIp );

            /**
             * if primary server fails to respond, perform secondary query
             */
            if ( ipsFromDns.size() == 0 ) {
                System.out.printf( "\t[%d/%d] dns query %s\n", i, dnsServers.size(), dns.secondaryIp );
                ipsFromDns = handleExceptionsGetIpsFromDns( res.url, dns.secondaryIp );
            }

            if ( ipsFromDns.size() == 0 ) {
                ++failedRequests;
            }

            /**
             * filter the duplicates
             * this will ignore load distribution of the resource
             */

            for ( int j = 0; j < ipsFromDns.size(); ++j ) {
                if ( resIps.contains( ipsFromDns.get( j ) ) ) {
                    ++duplicateIps;

                } else {
                    resIps.add( ipsFromDns.get( j ) );
                }
            }
        }

        for ( int i = 0; i < resIps.size(); ++i ) {
            System.out.println( "\tres out ip: " + resIps.get( i ) );
        }

        result = String.format( "%s;%s;%s;%d;%d",
                res.url,
                res.label,
                resIps.size() > 1 ? "Y" : "N",
                dnsServers.size() - failedRequests,
                duplicateIps
        );

        return result;
    }

    /**
     * utility method for catching exceptions while query process is executed
     * @param resourceUrl url of the network resource
     * @param dnsIp ip of dns server
     * @return list of ip addresses for url resource
     */
    private static ArrayList<String> handleExceptionsGetIpsFromDns( String resourceUrl, String dnsIp ) {

        ArrayList<String> ipsFromDns = new ArrayList<>();

        try {
            /**
             * actually get the resource ips
             */
            ipsFromDns = getIpsFromDns( resourceUrl, dnsIp );

        } catch ( IOException e ) {
            e.printStackTrace();

        } catch ( InterruptedException e ) {
            e.printStackTrace();
        }

        return ipsFromDns;
    }

    /**
     *
     * @param resourceUrl url of the network resource
     * @param dnsIp ip of dns server
     * @return list of
     * @throws IOException
     * @throws InterruptedException
     */
    private static ArrayList<String> getIpsFromDns( String resourceUrl, String dnsIp ) throws IOException, InterruptedException {

        ArrayList<String> ipsFromDns = new ArrayList<>();

        /**
         * create query process using nslookup
         */
        Runtime rt = Runtime.getRuntime();
        String command = String.format( "nslookup %s %s", resourceUrl, dnsIp );
        Process p = rt.exec( command );

        if ( SHOW_CONSOLE_LOG ) {
            System.out.println( "\t\tin> " + command );
        }

        /**
         * create thread listening to process output
         */
        Thread t = new Thread( () -> {
            /**
             * create process output stream
             */
            BufferedReader input = new BufferedReader( new InputStreamReader( p.getInputStream() ) );
            StringBuilder sb = new StringBuilder();
            String line = null;

            try {
                /**
                 * read process output line by line
                 * and start saving when it has valuable information
                 */
                boolean isOutputUseful = false;
                while ( ( line = input.readLine() ) != null ) {
                    if ( SHOW_CONSOLE_LOG ) {
                        System.out.println("\t\tout> " + line);
                    }

                    if ( line.toLowerCase().contains( "addresses:" ) ) {
                        isOutputUseful = true;
                    }

                    if ( isOutputUseful ) {
                        sb.append( line ).append( "\n" );
                    }
                }

                /**
                 * gather parsed ips
                 */
                ArrayList<String> ips = getParseIps( sb.toString() );
                for ( int j = 0; j < ips.size(); ++j ) {
                    ipsFromDns.add( ips.get( j ) );
                }

            } catch ( IOException e ) {
                e.printStackTrace();
            }

        } );
        t.start();

        /**
         * wait for dns server query process to end
         */
        p.waitFor();

        return ipsFromDns;
    }

    /**
     * use regexp to parse dns query process output
     * @param nslookupOutput raw command line process output
     * @return list of gathered ips
     */
    private static ArrayList<String> getParseIps( String nslookupOutput ) {
        // System.out.println( "getParseIps\n\n" );
        // System.out.println( nslookupOutput );

        ArrayList<String> allMatches = new ArrayList<>();

        String IP_ADDRESS_PATTERN = "\\d{1,3}.\\d{1,3}.\\d{1,3}.\\d{1,3}";

        Matcher matcher = Pattern.compile( IP_ADDRESS_PATTERN ).matcher( nslookupOutput );
        while ( matcher.find() ) {
            allMatches.add( matcher.group() );
            // System.out.println( "found " + matcher.group() );
        }

        // System.out.println( "\n\n" );
        return allMatches;
    }

    /**
     * clear file in given path
     * @param path path to file that will be cleared
     * @throws FileNotFoundException
     */
    private static void clearFile( String path ) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter( path );
        writer.print("");
        writer.close();
    }

    /**
     * Read content of file line by line and return urlResources
     * @param path path to given file
     * @return content of file as list of parsed lines
     * @throws IOException
     */
    private static ArrayList<UrlResource> readUrlList( String path ) throws IOException {
        // System.out.println( "readUrlList " + path );

        ArrayList<UrlResource> urlRes = new ArrayList<>();
        String[] urlLines = readLines( path );

        System.out.println( "\nUrls " + path );
        for ( int i = 0; i < urlLines.length; ++i ) {
            UrlResource url = UrlResource.fromCSV( urlLines[i] );
            urlRes.add( url );
            System.out.println( "\t#" + i + " " + url.url );
        }

        return urlRes;
    }

    /**
     * Read content of file line by line and return DnsServers
     * @param path path to given file
     * @return content of file as list of parsed lines
     * @throws IOException
     */
    private static ArrayList<DnsServer> readDnsList( String path ) throws IOException {
        // System.out.println( "readDnsList " + path );

        ArrayList<DnsServer> dnsServers = new ArrayList<>();
        String[] dnsLines = readLines( path );

        System.out.println( "\nDns " + path );
        for ( int i = 0; i < dnsLines.length; ++i ) {
            DnsServer srv = DnsServer.fromCSV( dnsLines[i] );
            dnsServers.add( srv );
            System.out.println( "\t#" + i + " " + srv.primaryIp + ",\t" + srv.secondaryIp );
        }

        return dnsServers;
    }

    /**
     * Read content of file line by line
     * @param filename path to file
     * @return content of file as list of parsed lines
     * @throws IOException
     */
    public static String[] readLines(String filename) throws IOException {

        List<String> lines = new ArrayList<>();
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            lines.add(line);
        }

        bufferedReader.close();

        return lines.toArray(new String[lines.size()]);
    }

    /**
     * method for experimenting and performing test of parsing ips from raw commandline output
     */
    static void testParsingIp() {
        String exampleOut1 = "" +
//                "Serwer:  resolver1.opendns.com\n" +
//                "Address:  208.67.222.222\n" +
//                "\n" +
//                "Nieautorytatywna odpowiedź:\n" +
//                "Nazwa:   aol.com\n" +
                "Addresses:  149.174.107.97\n" +
                "          207.200.74.38\n" +
                "          64.12.79.57\n" +
                "          149.174.110.102\n" +
                "          64.12.89.186";

        String exampleOut2 = "" +
//                "Serwer:  resolver1.ihgip.net\n" +
//                "Address:  84.200.69.80\n" +
//                "Aliases:  80.69.200.84.in-addr.arpa\n" +
//                "\n" +
//                "Nieautorytatywna odpowiedź:\n" +
//                "Nazwa:   aol.com\n" +
                "Addresses:  64.12.89.186\n" +
                "          149.174.110.102\n" +
                "          149.174.107.97\n" +
                "          64.12.79.57\n" +
                "          207.200.74.38";
        ArrayList<String> ips1 = getParseIps( exampleOut1 );
        System.out.println( "Ex1 " + ips1.size() );
        for ( int i = 0; i < ips1.size(); ++i ) {
            System.out.println( "ex1 " + i + " out ip: " + ips1.get( i ) );
        }

        ArrayList<String> ips2 = getParseIps( exampleOut2 );
        System.out.println( "Ex2 " + ips2.size() );
        for ( int i = 0; i < ips2.size(); ++i ) {
            System.out.println( "ex2 " + i + " out ip: " + ips2.get( i ) );
        }
    }
}