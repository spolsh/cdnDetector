/************************************************************************************

 Filename    :   Rdns.java
 Content     :   recusive dns server detector based on host via commandline process

 Created     :   January 17, 2016
 Authors     :   Lukasz Baranowski

 Copyright   :   Copyright 2015 PWr IiBI

 ************************************************************************************/

package com.spolsh;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Main class for Rdns
 */
public class Rdns {

    /**
     * paths to input and output files
     */
    private static final String FILE_RAW_DNS_LIST = "/home/lukasz/JavaProjects/cdnDetector/java8/data/recursiveDnsList.txt";
    private static final String FILE_RECURSIVE_DNS_LIST = "/home/lukasz/JavaProjects/cdnDetector/java8/data/recursiveDnsList.txt";
    private static final String TEST_DOMAIN = "www.cnn.com";

    public static void main( String[] args ) {

        System.out.println( "Rdns start" );
        System.out.println( "Working Directory = " + System.getProperty( "user.dir" ) );

        /**
         * get the program execution runtime
         */
        Runtime rt = Runtime.getRuntime();

        try {
            /**
             * read raw dns server list
             */
            System.out.println( "Reading " + FILE_RAW_DNS_LIST );
            String[] lines = readLines( FILE_RAW_DNS_LIST );

            /**
             * for every server check if is recursive
             */
            for ( int i = 0; i < lines.length; ++i ) {
                System.out.println( lines[i] );

                String tmp[] = lines[i].split(";");

                try {
                    /**
                     * create command line process with "host" command
                     */
                    Process p = rt.exec( "host " + TEST_DOMAIN + " " +  tmp[0]);

                    /**
                     * create thread reading output stream of commandline
                     */
                    new Thread(() -> {
                        /**
                         * create process output stream
                         */
                        BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
                        String line = null;

                        try {
                            /**
                             * read line by line
                             */
                            while ((line = input.readLine()) != null) {
                                System.out.println(line);
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }).start();

                    /**
                     * wait for process to complete
                     */
                    p.waitFor();

                } catch ( IOException e ) {
                    e.printStackTrace();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println( "Rdns end" );
    }

    /**
     * Helper merhod for reading file content to lines
     * @param filename path to file to open
     * @return file content in lines
     * @throws IOException
     */
    public static String[] readLines(String filename) throws IOException {

        List<String> lines = new ArrayList<>();
        FileReader fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            lines.add(line);
        }

        bufferedReader.close();

        return lines.toArray(new String[lines.size()]);
    }
}
