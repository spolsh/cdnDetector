package com.spolsh;

/**
 * Created by spolsh on 2016-01-16.
 */

/**
 * Structure for keeping url resource
 */
public class UrlResource {

    public final String url;
    public final String label;

    /**
     * Default constructor
     * @param url
     * @param label
     */
    public UrlResource( String url, String label ) {
        this.url = url;
        this.label = label;
    }

    /**
     * Create UrlResource object parsed from string line
     * @param csvLine input string
     * @return parsed object
     */
    public static UrlResource fromCSV( String csvLine ) {
        String[] parts = csvLine.split( ";" );
        return new UrlResource( parts[0], parts[1] );
    }
}
